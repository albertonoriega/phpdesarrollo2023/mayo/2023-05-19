<?php

namespace clases\elementos;

class Coche
{
    public  $marca;
    public  $modelo;
    public  $cilindrada;
    private  $bastidor;


    /**
     * __construct
     *
     * @param  string $marca Marca del coche
     * @param  string $modelo Modelo del coche
     * @return void
     */
    public function __construct($marca, $modelo)
    {
        $this->marca = $marca;
        $this->modelo = $modelo;
    }

    public function __toString()
    {
        $salida = "<ul>";
        $salida .= "<li> Marca: {$this->marca} </li>";
        $salida .= "<li> Modelo: {$this->modelo} </li>";
        $salida .= "<li> Cilindrada: {$this->cilindrada} </li>";
        $salida .= "<li> Bastidor: {$this->bastidor} </li>";
        $salida .= "</ul>";
        return $salida;
    }

    // GETTERS
    public function getMarca()
    {
        return $this->marca;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function getCilindrada()
    {
        return $this->cilindrada;
    }

    public function getBastidor()
    {
        return $this->bastidor;
    }

    // SETTERS
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function setCilindrada($cilindrada)
    {
        $this->cilindrada = $cilindrada;

        return $this;
    }

    public function setBastidor($bastidor)
    {
        if (!empty($bastidor)) {
            $this->bastidor = $bastidor;
        }
        return $this;
    }

    public function dibujarTabla()
    {
        ob_start();
?>
        <table border="1" style="text-align: center; border-collapse: collapse;">
            <thead>
                <tr>
                    <td>Marca</td>
                    <td>Modelo</td>
                    <td>Cilindrada</td>
                    <td>Bastidor</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> <?= $this->getMarca() ?></td>
                    <td> <?= $this->getModelo() ?></td>
                    <td> <?= $this->getCilindrada() ?></td>
                    <td> <?= $this->getBastidor() ?></td>
                </tr>
            </tbody>
        </table>
<?php
        return ob_get_clean();
    }
}
