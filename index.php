<?php

use clases\elementos\Coche;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});


$coche1 = new Coche('Seat', 'Toledo');

var_dump($coche1);

echo $coche1;

echo $coche1->dibujarTabla();

$coche1->setBastidor('123456');
echo $coche1->getBastidor();
$coche1->setCilindrada(100);

echo $coche1->dibujarTabla();
